package org.sda.springwebappexample.controller;

import org.sda.springwebappexample.model.User;
import org.sda.springwebappexample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String getUser(Model model) {
		User user1 = new User("Tibi", "Saska", "0012124545");
		User user2 = new User("Roxi", "Tat", "0051514563");

		model.addAttribute("user1", user1);
		model.addAttribute("user2", user2);
		return "userPage";
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public String getUserById(@PathVariable(value = "id") int id, Model model) {
		;
		User user = new User();
		user.setId(id);
		user.setFirstName("mihai");
		user.setLastName("alexandru");
		user.setPhone("125634");

		model.addAttribute("user", user);
		return "userPage";

	}
	
	@RequestMapping(value = "/users/{id}/addresses", method = RequestMethod.GET)
	public String getAddressesForUserId(Model model,@PathVariable(value="id") int id) {
		User user = userService.getUserByid(id);
		
		model.addAttribute("addresses", user.getAddresses());
		
		return "userAddressPage";
	
	}
}
