package org.sda.springwebappexample.service;

import java.util.Arrays;
import java.util.List;

import org.sda.springwebappexample.model.Address;
import org.sda.springwebappexample.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	public User getUserByid(int id) {
		User user = new User();
		user.setId(id);
		user.setFirstName("tudor");
		user.setLastName("alexandru");
		user.setPhone("4568545");
		
		Address address1 = new Address(1,"cluj", "romania", "memo");
		Address address2 = new Address(2,"tg mures", "romania", "dorobanti");
		
		user.setAddresses(Arrays.asList(address1, address2));
		
		return user;
	}
	
	public Address getAddressByIdFromList(List<Address> addresses, int id){
		for (Address address: addresses) {
			if (address.getId()==id) {
				return address;
			}
		}
		return null;	
	
	}
}
